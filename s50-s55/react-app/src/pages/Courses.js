//import coursesData from '../data/coursesData';
import CourseCard from "../components/CourseCard";
import { useState, useEffect } from "react";

export default function Courses() {
  const [courses, setCourses] = useState([]);

  // console.log(coursesData);
  // console.log(coursesData[0]);

  // const courses = coursesData.map(course => {
  // 	return (
  // 		<CourseCard key={course.id} course={course} />
  // 	);
  // });

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/active-courses`)
      .then((res) => res.json())
      .then((data) => {
        setCourses(
          data.map((course) => {
            return <CourseCard key={course._id} course={course} />;
          })
        );
      });
  }, []);

  return <>{courses}</>;
}
