const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.getElementById('txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

const updateFullName = (event) => {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}

txtFirstName.addEventListener('keyup', (event) => updateFullName(event));

txtLastName.addEventListener('keyup', (event) => updateFullName(event));